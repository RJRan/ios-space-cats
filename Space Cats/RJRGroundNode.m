//
//  RJRGroundNode.m
//  Space Cats
//
//  Created by Robert Randell on 22/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRGroundNode.h"
#import "RJRUtil.h"

@implementation RJRGroundNode

+ (instancetype)groundWithSize:(CGSize)size {
    
    RJRGroundNode *ground = [self spriteNodeWithColor:[SKColor clearColor] size:size];
    ground.name = @"Ground";
    ground.position = CGPointMake(size.width/2, size.height/2);
    [ground setupPhysicsBody];
    return ground;
    
}

- (void)setupPhysicsBody {
    
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.frame.size];
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.dynamic = NO;
    self.physicsBody.categoryBitMask = RJRCollisionCategoryGround;
    self.physicsBody.collisionBitMask = RJRCollisionCategoryDebris;
    self.physicsBody.contactTestBitMask = RJRCollisionCategoryEnemy;
    
}

@end