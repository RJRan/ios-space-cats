//
//  RJRGameOverNode.h
//  Space Cats
//
//  Created by Robert Randell on 23/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RJRGameOverNode : SKNode

+ (instancetype)gameOverAtPosition:(CGPoint)position;

- (void)performAnimation;

@end
