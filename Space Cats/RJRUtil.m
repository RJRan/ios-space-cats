//
//  RJRUtil.m
//  Space Cats
//
//  Created by Robert Randell on 21/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRUtil.h"

@implementation RJRUtil

+ (NSInteger)randomWithMin:(NSInteger)min max:(NSInteger)max {
    return arc4random()%(max - min) + min;
}

@end
