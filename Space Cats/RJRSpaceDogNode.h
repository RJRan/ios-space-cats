//
//  RJRSpaceDogNode.h
//  Space Cats
//
//  Created by Robert Randell on 22/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef NS_ENUM(NSUInteger, RJRSpaceDogType) {
    RJRSpaceDogTypeA = 0,
    RJRSpaceDogTypeB = 1
};

@interface RJRSpaceDogNode : SKSpriteNode

+ (instancetype)spaceDogOfType:(RJRSpaceDogType)type;

@end
