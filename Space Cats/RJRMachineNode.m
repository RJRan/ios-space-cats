//
//  RJRMachineNode.m
//  Space Cats
//
//  Created by Robert Randell on 21/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRMachineNode.h"

@implementation RJRMachineNode

+ (instancetype)machineAtPosition:(CGPoint)position {
    
    RJRMachineNode *machine = [self spriteNodeWithImageNamed:@"machine_1"];
    machine.position = position;
    machine.name = @"Machine";
    machine.anchorPoint = CGPointMake(0.5, 0);
    machine.zPosition = 8;
    
    NSArray *textures = @[[SKTexture textureWithImageNamed:@"machine_1"], [SKTexture textureWithImageNamed:@"machine_2"]];
    
    SKAction *machineAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
    SKAction *machineRepeat = [SKAction repeatActionForever:machineAnimation];
    
    [machine runAction:machineRepeat];
    
    return machine;
    
}

@end
