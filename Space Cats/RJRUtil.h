//
//  RJRUtil.h
//  Space Cats
//
//  Created by Robert Randell on 21/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <Foundation/Foundation.h>

static const int RJRProjectileSpeed = 400;
static const int RJRSpaceDogMinSpeed = -100;
static const int RJRSpaceDogMaxSpeed = - 50;
static const int RJRMaxLives = 4;
static const int RJRPointsPerHit = 100;

typedef NS_OPTIONS(uint32_t, RJRCollisionCategory) {
    RJRCollisionCategoryEnemy = 1 << 0, //0000
    RJRCollisionCategoryProjectile = 1 << 1, //0010
    RJRCollisionCategoryDebris = 1 << 2, //0100
    RJRCollisionCategoryGround = 1 << 3 //1000
};

@interface RJRUtil : NSObject

+ (NSInteger)randomWithMin:(NSInteger)min max:(NSInteger)max;

@end
