//
//  RJRGroundNode.h
//  Space Cats
//
//  Created by Robert Randell on 22/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RJRGroundNode : SKSpriteNode

+ (instancetype)groundWithSize:(CGSize)size;

@end
