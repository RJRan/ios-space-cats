//
//  RJRGamePlayScene.m
//  Space Cats
//
//  Created by Robert Randell on 21/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRGamePlayScene.h"
#import "RJRMachineNode.h"
#import "RJRSpaceCatNode.h"
#import "RJRProjectileNode.h"
#import "RJRSpaceDogNode.h"
#import "RJRGroundNode.h"
#import "RJRUtil.h"
#import "RJRHudNode.h"
#import "RJRGameOverNode.h"
#import <AVFoundation/AVFoundation.h>

@interface RJRGamePlayScene ()

@property (nonatomic) NSTimeInterval lastUpdateTimeInterval;
@property (nonatomic) NSTimeInterval timeSinceEnemyAdded;
@property (nonatomic) NSTimeInterval totalGameTime;
@property (nonatomic) NSInteger minSpeed;
@property (nonatomic) NSTimeInterval addEnemyTimeInterval;

@property (nonatomic) SKAction *damageSFX;
@property (nonatomic) SKAction *explodeSFX;
@property (nonatomic) SKAction *laserSFX;

@property (nonatomic) AVAudioPlayer *backgroundMusic;
@property (nonatomic) AVAudioPlayer *gameOverMusic;

@property (nonatomic) BOOL gameOver;
@property (nonatomic) BOOL restart;
@property (nonatomic) BOOL gameOverDisplayed;

@end

@implementation RJRGamePlayScene

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        self.lastUpdateTimeInterval = 0;
        self.timeSinceEnemyAdded = 0;
        self.addEnemyTimeInterval = 1.5;
        self.totalGameTime = 0;
        self.minSpeed = RJRSpaceDogMinSpeed;
        self.restart = NO;
        self.gameOver = NO;
        self.gameOverDisplayed = NO;
        
        /* Setup your scene here */
        SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"background_1"];
        background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:background];
        
        RJRMachineNode *machine = [RJRMachineNode machineAtPosition:CGPointMake(CGRectGetMidX(self.frame), 12)];
        [self addChild:machine];
        
        RJRSpaceCatNode *spaceCat = [RJRSpaceCatNode spaceCatAtPosition:CGPointMake(machine.position.x, machine.position.y - 2)];
        [self addChild:spaceCat];
        
        //[self addSpaceDog];
        
        self.physicsWorld.gravity = CGVectorMake(0, -9.8);
        self.physicsWorld.contactDelegate = self;
        
        RJRGroundNode *ground = [RJRGroundNode groundWithSize:CGSizeMake(self.frame.size.width, 22)];
        
        [self addChild:ground];
        [self setupSounds];
        
        RJRHudNode *hud = [RJRHudNode hudAtPosition:CGPointMake(0, self.frame.size.height - 20) inFrame:self.frame];
        [self addChild:hud];
    }
    return self;
}

- (void)setupSounds {
    self.damageSFX = [SKAction playSoundFileNamed:@"Damage.caf" waitForCompletion:NO];
    self.explodeSFX = [SKAction playSoundFileNamed:@"Explode.caf" waitForCompletion:NO];
    self.laserSFX = [SKAction playSoundFileNamed:@"Laser.caf" waitForCompletion:NO];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Gameplay" withExtension:@"mp3"];
    
    self.backgroundMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    self.backgroundMusic.numberOfLoops = -1;
    [self.backgroundMusic prepareToPlay];
    
    NSURL *gameOverUrl = [[NSBundle mainBundle] URLForResource:@"GameOver" withExtension:@"mp3"];
    
    self.gameOverMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:gameOverUrl error:nil];
    self.gameOverMusic.numberOfLoops = 1;
    [self.gameOverMusic prepareToPlay];

}

- (void)didMoveToView:(SKView *)view {
    [self.backgroundMusic play];
}

- (void)update:(NSTimeInterval)currentTime {
    
    if (self.lastUpdateTimeInterval) {
        self.timeSinceEnemyAdded += currentTime - self.lastUpdateTimeInterval;
        self.totalGameTime += currentTime - self.lastUpdateTimeInterval;
    }
    if (self.timeSinceEnemyAdded > self.addEnemyTimeInterval && !self.gameOver) {
        [self addSpaceDog];
        self.timeSinceEnemyAdded = 0;
    }
    self.lastUpdateTimeInterval = currentTime;
    
    if (self.totalGameTime > 240) {
        self.addEnemyTimeInterval = 0.5;
        self.minSpeed = -160;
    }
    else if (self.totalGameTime > 180) {
        self.addEnemyTimeInterval = 0.65;
        self.minSpeed = -150;
    }
    else if (self.totalGameTime > 120) {
        self.addEnemyTimeInterval = 0.75;
        self.minSpeed = -125;
    }
    else if (self.totalGameTime > 30) {
        self.addEnemyTimeInterval = 1.0;
        self.minSpeed = -100;
    }
    
    if (self.gameOver && !self.gameOverDisplayed) {
        [self performGameOver];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(!self.gameOver) {
        for (UITouch *touch in touches) {
            CGPoint position = [touch locationInNode:self];
            [self shootProjectileTowardsPosition:position];
        }
    }
    else if (self.restart) {
        
        for (SKNode *node in [self children]) {
            [node removeFromParent];
        }
        
        RJRGamePlayScene *scene = [RJRGamePlayScene sceneWithSize:self.view.bounds.size];
        [self.view presentScene:scene];
    }
}

- (void)performGameOver {
    
    RJRGameOverNode *gameOver = [RJRGameOverNode gameOverAtPosition:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
    [self addChild:gameOver];
    
    self.restart = YES;
    self.gameOverDisplayed = YES;
    [gameOver performAnimation];
    
    [self.backgroundMusic stop];
    [self.gameOverMusic play];
}

- (void)shootProjectileTowardsPosition:(CGPoint)position {
    
    RJRSpaceCatNode *spaceCat = (RJRSpaceCatNode *)[self childNodeWithName:@"SpaceCat"];
    [spaceCat performTap];
    
    RJRMachineNode *machine = (RJRMachineNode *)[self childNodeWithName:@"Machine"];
    
    RJRProjectileNode *projectile = [RJRProjectileNode projectileAtPosition:CGPointMake(machine.position.x, machine.position.y + machine.frame.size.height - 15)];
    [self addChild:projectile];
    [projectile moveTowardsPosition:position];
    
    [self runAction:self.laserSFX];
    
}

- (void)addSpaceDog {
    
    NSUInteger randomSpaceDog = [RJRUtil randomWithMin:0 max:2];
    
    RJRSpaceDogNode *spaceDog = [RJRSpaceDogNode spaceDogOfType:randomSpaceDog];
    
    float dy = [RJRUtil randomWithMin:RJRSpaceDogMinSpeed max:RJRSpaceDogMaxSpeed];
    float x = [RJRUtil randomWithMin:10 + spaceDog.size.width max:self.frame.size.width - spaceDog.size.width - 10];
    float y = self.frame.size.height + spaceDog.size.height;
    
    spaceDog.physicsBody.velocity = CGVectorMake(0, dy);
    spaceDog.position = CGPointMake(x, y);
    
    [self addChild:spaceDog];
    
    /*
    RJRSpaceDogNode *spaceDogA = [RJRSpaceDogNode spaceDogOfType:RJRSpaceDogTypeA];
    spaceDogA.position = CGPointMake(100, 300);
    [self addChild:spaceDogA];
    
    RJRSpaceDogNode *spaceDogB = [RJRSpaceDogNode spaceDogOfType:RJRSpaceDogTypeB];
    spaceDogB.position = CGPointMake(200, 300);
    [self addChild:spaceDogB];
    */
}

- (void)didBeginContact:(SKPhysicsContact *)contact {
    
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if (firstBody.categoryBitMask == RJRCollisionCategoryEnemy && secondBody.categoryBitMask == RJRCollisionCategoryProjectile) {
        NSLog(@"BAM!");
        
        RJRSpaceDogNode *spaceDog = (RJRSpaceDogNode *)firstBody.node;
        RJRProjectileNode *projectile = (RJRProjectileNode *)secondBody.node;
        
        [self addPoints:RJRPointsPerHit];
        
        [self runAction:self.explodeSFX];
        [spaceDog removeFromParent];
        [projectile removeFromParent];

    }
    else if (firstBody.categoryBitMask == RJRCollisionCategoryEnemy && secondBody.categoryBitMask == RJRCollisionCategoryGround) {
        NSLog(@"Hit Ground!");
        
        RJRSpaceDogNode *spaceDog = (RJRSpaceDogNode *)firstBody.node;
        [self runAction:self.damageSFX];
        [spaceDog removeFromParent];
        [self loseLife];

    }
    [self createDebrisAtPosition:contact.contactPoint];

}

- (void)loseLife {
    
    RJRHudNode *hud = (RJRHudNode *)[self childNodeWithName:@"HUD"];
    self.gameOver = [hud loseLife];
}

- (void)addPoints:(NSInteger)points {
    
    RJRHudNode *hud = (RJRHudNode *)[self childNodeWithName:@"HUD"];
    [hud addPoints:points];
}

- (void)createDebrisAtPosition:(CGPoint)position {
    
    NSInteger numberOfPieces = [RJRUtil randomWithMin:5 max:20];
    
    for (int i = 0; i < numberOfPieces; i++) {
        NSInteger randomPiece = [RJRUtil randomWithMin:1 max:4];
        NSString *imageName = [NSString stringWithFormat:@"debri_%d", randomPiece];
        
        SKSpriteNode *debris = [SKSpriteNode spriteNodeWithImageNamed:imageName];
        debris.position = position;
        [self addChild:debris];
        
        debris.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:debris.frame.size];
        debris.physicsBody.categoryBitMask = RJRCollisionCategoryDebris;
        debris.physicsBody.contactTestBitMask = 0;
        debris.physicsBody.collisionBitMask = RJRCollisionCategoryGround | RJRCollisionCategoryDebris;
        debris.name = @"Debris";
        
        debris.physicsBody.velocity = CGVectorMake([RJRUtil randomWithMin:-150 max:150], [RJRUtil randomWithMin:150 max:350]);
        
        [debris runAction:[SKAction waitForDuration:2.0] completion:^{
            [debris removeFromParent];
        }];
    }
    
    NSString *explosionPath = [[NSBundle mainBundle] pathForResource:@"Explosion" ofType:@"sks"];
    SKEmitterNode *explosion = [NSKeyedUnarchiver unarchiveObjectWithFile:explosionPath];
    
    explosion.position = position;
    [self addChild:explosion];
    
    [explosion runAction:[SKAction waitForDuration:2.0] completion:^{
        [explosion removeFromParent];
    }];
}

@end
