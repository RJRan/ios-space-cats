//
//  RJRSpaceDogNode.m
//  Space Cats
//
//  Created by Robert Randell on 22/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRSpaceDogNode.h"
#import "RJRUtil.h"

@implementation RJRSpaceDogNode

+ (instancetype)spaceDogOfType:(RJRSpaceDogType)type {
    
    RJRSpaceDogNode *spaceDog;
    NSArray *textures;
    
    if (type == RJRSpaceDogTypeA) {
        spaceDog = [self spriteNodeWithImageNamed:@"spacedog_A_1"];
        textures = @[[SKTexture textureWithImageNamed:@"spacedog_A_1"],
                     [SKTexture textureWithImageNamed:@"spacedog_A_2"],
                     [SKTexture textureWithImageNamed:@"spacedog_A_3"]];
    }
    else {
        spaceDog = [self spriteNodeWithImageNamed:@"spacedog_B_1"];
        textures = @[[SKTexture textureWithImageNamed:@"spacedog_B_1"],
                     [SKTexture textureWithImageNamed:@"spacedog_B_2"],
                     [SKTexture textureWithImageNamed:@"spacedog_B_3"],
                     [SKTexture textureWithImageNamed:@"spacedog_B_4"]];
    }
    
    float scale = [RJRUtil randomWithMin:85 max:100] / 100.0f;
    spaceDog.xScale = scale;
    spaceDog.yScale = scale;
    
    SKAction *animation = [SKAction animateWithTextures:textures timePerFrame:0.1];
    [spaceDog runAction:[SKAction repeatActionForever:animation]];
    [spaceDog setupPhysicsBody];
    
    return spaceDog;
}

- (void)setupPhysicsBody {
    
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.frame.size];
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.categoryBitMask = RJRCollisionCategoryEnemy;
    self.physicsBody.collisionBitMask = 0;
    self.physicsBody.contactTestBitMask = RJRCollisionCategoryProjectile | RJRCollisionCategoryGround; // 0010 | 1000 = 1010
}

@end
