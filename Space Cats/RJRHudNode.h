//
//  RJRHudNode.h
//  Space Cats
//
//  Created by Robert Randell on 23/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RJRHudNode : SKNode

@property (nonatomic) NSInteger lives;
@property (nonatomic) NSInteger score;

+ (instancetype)hudAtPosition:(CGPoint)position inFrame:(CGRect)frame;
- (void)addPoints:(NSInteger)points;
- (BOOL)loseLife;

@end
