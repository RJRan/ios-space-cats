//
//  RJRSpaceCat.m
//  Space Cats
//
//  Created by Robert Randell on 21/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRSpaceCatNode.h"

@interface RJRSpaceCatNode ()

@property (nonatomic) SKAction *tapAction;

@end

@implementation RJRSpaceCatNode

+ (instancetype)spaceCatAtPosition:(CGPoint)position {
    
    RJRSpaceCatNode *spaceCat = [self spriteNodeWithImageNamed:@"spacecat_1"];
    spaceCat.position = position;
    spaceCat.anchorPoint = CGPointMake(0.5, 0);
    spaceCat.name = @"SpaceCat";
    spaceCat.zPosition = 9;
    
    return spaceCat;
}

- (void)performTap {
    [self runAction:self.tapAction];
}

- (SKAction *)tapAction {
    
    if (_tapAction != nil) {
        return _tapAction;
    }
    NSArray *textures = @[[SKTexture textureWithImageNamed:@"spacecat_2"], [SKTexture textureWithImageNamed:@"spacecat_1"]];
    _tapAction = [SKAction animateWithTextures:textures timePerFrame:0.25];
    
    return _tapAction;
    
}

@end
