//
//  RJRProjectileNode.h
//  Space Cats
//
//  Created by Robert Randell on 21/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface RJRProjectileNode : SKSpriteNode

+ (instancetype)projectileAtPosition:(CGPoint)position;
- (void)moveTowardsPosition:(CGPoint)position;

@end
